﻿namespace Teste.Sequoia.Domain.ViewModels
{
    public class FilmeViewModel
    {
        public int FilmeId { get; set; }
        public string Nome { get; set; }
        public int GeneroId { get; set; }
        public GeneroViewModel Genero { get; set; }
        public int AutorId { get; set; }
        public AutorViewModel Autor { get; set; }
    }
}