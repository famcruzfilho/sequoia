﻿using System.Collections.Generic;

namespace Teste.Sequoia.Domain.Contracts.AppServices.Base
{
    public interface IBaseAppService<TEntity> where TEntity : class
    {
        void Criar(TEntity obj);
        void Criar(IEnumerable<TEntity> objs);
        TEntity ObterPorId(int id);
        IEnumerable<TEntity> ObterTodos();
        void Atualizar(TEntity obj);
        void DeletarPorId(int id);
        void Deletar(TEntity obj);
    }
}