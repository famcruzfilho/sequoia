﻿using System.Collections.Generic;
using System.Data.Entity;
using Teste.Sequoia.Domain.Contracts.Repositories.Base;
using Teste.Sequoia.Repository.Data.Context;

namespace Teste.Sequoia.Repository.Data.Repositories.Base
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        private readonly DataContext _context;

        public BaseRepository(DataContext context)
        {
            _context = context;
        }

        public void Atualizar(TEntity obj)
        {
            _context.Entry(obj).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public void Criar(TEntity obj)
        {
            _context.Set<TEntity>().Add(obj);
            _context.SaveChanges();
        }

        public void Criar(IEnumerable<TEntity> objs)
        {
            foreach (TEntity obj in objs)
            {
                _context.Set<TEntity>().Add(obj);
                _context.SaveChanges();
            }
        }

        public void DeletarPorId(int id)
        {
            var obj = _context.Set<TEntity>().Find(id);
            _context.Set<TEntity>().Remove(obj);
            _context.SaveChanges();
        }

        public void Deletar(TEntity obj)
        {
            _context.Set<TEntity>().Remove(obj);
            _context.SaveChanges();
        }

        public TEntity ObterPorId(int id)
        {
            return _context.Set<TEntity>().Find(id);
        }

        public IEnumerable<TEntity> ObterTodos()
        {
            return _context.Set<TEntity>();
        }
    }
}