﻿using System.Collections.Generic;

namespace Teste.Sequoia.Domain.Models
{
    public class Autor
    {
        public int AutorId { get; set; }
        public string Nome { get; set; }
        public ICollection<Filme> Filmes { get; set; }
    }
}