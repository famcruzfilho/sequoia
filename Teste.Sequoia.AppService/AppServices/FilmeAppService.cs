﻿using System.Collections.Generic;
using Teste.Sequoia.AppService.AppServices.Base;
using Teste.Sequoia.Domain.Contracts.AppServices;
using Teste.Sequoia.Domain.Contracts.Services;
using Teste.Sequoia.Domain.Models;

namespace Teste.Sequoia.AppService.AppServices
{
    public class FilmeAppService : BaseAppService<Filme>, IFilmeAppService
    {
        private readonly IFilmeService _filmeService;

        public FilmeAppService(IFilmeService filmeService) : base(filmeService)
        {
            _filmeService = filmeService;
        }

        public void AdicionarOuAtualizarViaProcedure(string procedure, Dictionary<string, string> parametros)
        {
            _filmeService.AdicionarOuAtualizarViaProcedure(procedure, parametros);
        }
    }
}