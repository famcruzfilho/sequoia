﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Teste.Sequoia.Domain.Models;

namespace Teste.Sequoia.Repository.Data.Mappings
{
    public class GeneroMap : EntityTypeConfiguration<Genero>
    {
        public GeneroMap()
        {
            ToTable("Generos");

            Property(x => x.GeneroId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.Nome)
                .HasMaxLength(250)
                .IsRequired();
        }
    }
}