﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Teste.Sequoia.Domain.Models;

namespace Teste.Sequoia.Repository.Data.Mappings
{
    public class FilmeMap : EntityTypeConfiguration<Filme>
    {
        public FilmeMap()
        {
            ToTable("Filmes");

            Property(x => x.FilmeId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.Nome)
                .HasMaxLength(250)
                .IsRequired();

            HasRequired(x => x.Genero);

            HasRequired(x => x.Autor);
        }
    }
}