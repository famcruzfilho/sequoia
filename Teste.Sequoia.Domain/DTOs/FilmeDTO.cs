﻿namespace Teste.Sequoia.Domain.DTOs
{
    public class FilmeDTO
    {
        public int FilmeId { get; set; }
        public string Nome { get; set; }
        public int GeneroId { get; set; }
        public GeneroDTO Genero { get; set; }
        public int AutorId { get; set; }
        public AutorDTO Autor { get; set; }
    }
}