﻿using System.Collections.Generic;
using Teste.Sequoia.Domain.Contracts.Repositories.Base;
using Teste.Sequoia.Domain.Contracts.Services.Base;

namespace Teste.Sequoia.Business.Services.Base
{
    public class BaseService<TEntity> : IBaseService<TEntity> where TEntity : class
    {
        private readonly IBaseRepository<TEntity> _repository;

        public BaseService(IBaseRepository<TEntity> repository)
        {
            _repository = repository;
        }

        public void Atualizar(TEntity obj)
        {
            _repository.Atualizar(obj);
        }

        public void Criar(TEntity obj)
        {
            _repository.Criar(obj);
        }

        public void Criar(IEnumerable<TEntity> objs)
        {
            _repository.Criar(objs);
        }

        public void Deletar(TEntity obj)
        {
            _repository.Deletar(obj);
        }

        public void DeletarPorId(int id)
        {
            _repository.DeletarPorId(id);
        }

        public TEntity ObterPorId(int id)
        {
            return _repository.ObterPorId(id);
        }

        public IEnumerable<TEntity> ObterTodos()
        {
            return _repository.ObterTodos();
        }
    }
}