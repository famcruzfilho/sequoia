﻿using System.Collections.Generic;
using Teste.Sequoia.Domain.Contracts.Repositories;
using Teste.Sequoia.Domain.Models;
using Teste.Sequoia.Repository.Data.Context;
using Teste.Sequoia.Repository.Data.Procedures;
using Teste.Sequoia.Repository.Data.Repositories.Base;

namespace Teste.Sequoia.Repository.Data.Repositories
{
    public class GeneroRepository : BaseRepository<Genero>, IGeneroRepository
    {
        private readonly DataContext _context;

        public GeneroRepository(DataContext context) : base(context)
        {
            _context = context;
        }

        public void AdicionarOuAtualizarViaProcedure(string procedure, Dictionary<string, string> parametros)
        {
            ExecutarProcedure.Executar(procedure, parametros);
        }
    }
}