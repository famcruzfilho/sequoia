﻿using System.Collections.Generic;
using Teste.Sequoia.Domain.Contracts.AppServices.Base;
using Teste.Sequoia.Domain.Models;

namespace Teste.Sequoia.Domain.Contracts.AppServices
{
    public interface IAutorAppService : IBaseAppService<Autor>
    {
        void AdicionarOuAtualizarViaProcedure(string procedure, Dictionary<string, string> parametros);
    }
}