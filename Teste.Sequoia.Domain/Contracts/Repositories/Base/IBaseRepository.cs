﻿using System.Collections.Generic;

namespace Teste.Sequoia.Domain.Contracts.Repositories.Base
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        void Criar(TEntity obj);
        void Criar(IEnumerable<TEntity> objs);
        TEntity ObterPorId(int id);
        IEnumerable<TEntity> ObterTodos();
        void Atualizar(TEntity obj);
        void DeletarPorId(int id);
        void Deletar(TEntity obj);
    }
}