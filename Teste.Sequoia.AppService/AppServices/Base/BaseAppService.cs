﻿using System.Collections.Generic;
using Teste.Sequoia.Domain.Contracts.AppServices.Base;
using Teste.Sequoia.Domain.Contracts.Services.Base;

namespace Teste.Sequoia.AppService.AppServices.Base
{
    public class BaseAppService<TEntity> : IBaseAppService<TEntity> where TEntity : class
    {
        private readonly IBaseService<TEntity> _service;

        public BaseAppService(IBaseService<TEntity> service)
        {
            _service = service;
        }

        public void Atualizar(TEntity obj)
        {
            _service.Atualizar(obj);
        }

        public void Criar(TEntity obj)
        {
            _service.Criar(obj);
        }

        public void Criar(IEnumerable<TEntity> objs)
        {
            _service.Criar(objs);
        }

        public void Deletar(TEntity obj)
        {
            _service.Deletar(obj);
        }

        public void DeletarPorId(int id)
        {
            _service.DeletarPorId(id);
        }

        public TEntity ObterPorId(int id)
        {
            return _service.ObterPorId(id);
        }

        public IEnumerable<TEntity> ObterTodos()
        {
            return _service.ObterTodos();
        }
    }
}