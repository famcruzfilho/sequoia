﻿using System.Collections.Generic;
using Teste.Sequoia.Business.Services.Base;
using Teste.Sequoia.Domain.Contracts.Repositories;
using Teste.Sequoia.Domain.Contracts.Services;
using Teste.Sequoia.Domain.Models;

namespace Teste.Sequoia.Business.Services
{
    public class FilmeService : BaseService<Filme>, IFilmeService
    {
        private readonly IFilmeRepository _filmeRepository;

        public FilmeService(IFilmeRepository filmeRepository) : base(filmeRepository)
        {
            _filmeRepository = filmeRepository;
        }

        public void AdicionarOuAtualizarViaProcedure(string procedure, Dictionary<string, string> parametros)
        {
            _filmeRepository.AdicionarOuAtualizarViaProcedure(procedure, parametros);
        }
    }
}