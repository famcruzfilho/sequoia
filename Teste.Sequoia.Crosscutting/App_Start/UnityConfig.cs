using System;
using Teste.Sequoia.AppService.AppServices;
using Teste.Sequoia.Business.Services;
using Teste.Sequoia.Domain.Contracts.AppServices;
using Teste.Sequoia.Domain.Contracts.Repositories;
using Teste.Sequoia.Domain.Contracts.Services;
using Teste.Sequoia.Domain.DTOs;
using Teste.Sequoia.Domain.Models;
using Teste.Sequoia.Domain.ViewModels;
using Teste.Sequoia.Repository.Data.Context;
using Teste.Sequoia.Repository.Data.Repositories;
using Unity;

namespace Teste.Sequoia.Crosscutting
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            //Context
            container.RegisterType<DataContext, DataContext>();

            //Models
            container.RegisterType<Autor, Autor>();
            container.RegisterType<Filme, Filme>();
            container.RegisterType<Genero, Genero>();

            //ViewModels
            container.RegisterType<AutorViewModel, AutorViewModel>();
            container.RegisterType<FilmeViewModel, FilmeViewModel>();
            container.RegisterType<GeneroViewModel, GeneroViewModel>();

            //DTO's
            container.RegisterType<AutorDTO, AutorDTO>();
            container.RegisterType<FilmeDTO, FilmeDTO>();
            container.RegisterType<GeneroDTO, GeneroDTO>();

            //AppServices
            container.RegisterType<IAutorAppService, AutorAppService>();
            container.RegisterType<IFilmeAppService, FilmeAppService>();
            container.RegisterType<IGeneroAppService, GeneroAppService>();

            //Services
            container.RegisterType<IAutorService, AutorService>();
            container.RegisterType<IFilmeService, FilmeService>();
            container.RegisterType<IGeneroService, GeneroService>();

            //Repositories
            container.RegisterType<IAutorRepository, AutorRepository>();
            container.RegisterType<IFilmeRepository, FilmeRepository>();
            container.RegisterType<IGeneroRepository, GeneroRepository>();
        }
    }
}