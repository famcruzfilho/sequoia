﻿namespace Teste.Sequoia.Domain.Models
{
    public class Filme
    {
        public int FilmeId { get; set; }
        public string Nome { get; set; }
        public int GeneroId { get; set; }
        public Genero Genero { get; set; }
        public int AutorId { get; set; }
        public Autor Autor { get; set; }
    }
}