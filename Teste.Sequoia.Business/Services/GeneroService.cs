﻿using System.Collections.Generic;
using Teste.Sequoia.Business.Services.Base;
using Teste.Sequoia.Domain.Contracts.Repositories;
using Teste.Sequoia.Domain.Contracts.Services;
using Teste.Sequoia.Domain.Models;

namespace Teste.Sequoia.Business.Services
{
    public class GeneroService : BaseService<Genero>, IGeneroService
    {
        private readonly IGeneroRepository _generoRepository;

        public GeneroService(IGeneroRepository generoRepository) : base(generoRepository)
        {
            _generoRepository = generoRepository;
        }

        public void AdicionarOuAtualizarViaProcedure(string procedure, Dictionary<string, string> parametros)
        {
            _generoRepository.AdicionarOuAtualizarViaProcedure(procedure, parametros);
        }
    }
}