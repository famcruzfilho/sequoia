﻿function Cancelar() {
    $('#AutorId').val("");
    $('#Nome').val("");
}

function Salvar() {
    $.ajax({
        url: urlSalvar,
        data: $('#formInputAutor').serialize(),
        type: 'POST',
        success: function (result) {
            if (result.Sucesso) {
                alert(result.MensagemRetorno);
            }
            else {
                alert("Dados salvos com falha");
            }
        }
    });
}