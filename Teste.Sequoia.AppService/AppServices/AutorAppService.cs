﻿using System.Collections.Generic;
using Teste.Sequoia.AppService.AppServices.Base;
using Teste.Sequoia.Domain.Contracts.AppServices;
using Teste.Sequoia.Domain.Contracts.Services;
using Teste.Sequoia.Domain.Models;

namespace Teste.Sequoia.AppService.AppServices
{
    public class AutorAppService : BaseAppService<Autor>, IAutorAppService
    {
        private readonly IAutorService _autorService;

        public AutorAppService(IAutorService autorService) : base(autorService)
        {
            _autorService = autorService;
        }

        public void AdicionarOuAtualizarViaProcedure(string procedure, Dictionary<string, string> parametros)
        {
            _autorService.AdicionarOuAtualizarViaProcedure(procedure, parametros);
        }
    }
}