﻿namespace Teste.Sequoia.Domain.ViewModels
{
    public class GeneroViewModel
    {
        public int GeneroId { get; set; }
        public string Nome { get; set; }
    }
}