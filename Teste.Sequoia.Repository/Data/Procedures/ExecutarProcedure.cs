﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Teste.Sequoia.Repository.Data.Procedures
{
    public static class ExecutarProcedure
    {
        private static string connectionString = "Data Source=localhost;Initial Catalog=Cinema;User ID=sa;Password=Em0t10n%";

        public static void Executar(string procedure, Dictionary<string, string> parametros)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                try
                {
                    var valor = (dynamic)null;
                    SqlCommand command = new SqlCommand(procedure, conn);
                    command.CommandType = CommandType.StoredProcedure;
                    foreach (KeyValuePair<string, string> item in parametros)
                    {
                        if(item.Key.Contains("Id"))
                        {
                            valor = Convert.ToInt32(item.Value);
                            command.Parameters.Add(new SqlParameter(string.Format("@{0}", item.Key), SqlDbType.Int)).Value = valor;
                        }
                        else
                        {
                            valor = item.Value.ToString();
                            command.Parameters.AddWithValue(string.Format("@{0}", item.Key), valor);
                        }
                    }
                    conn.Open();
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
    }
}