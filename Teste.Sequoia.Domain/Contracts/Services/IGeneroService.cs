﻿using System.Collections.Generic;
using Teste.Sequoia.Domain.Contracts.Services.Base;
using Teste.Sequoia.Domain.Models;

namespace Teste.Sequoia.Domain.Contracts.Services
{
    public interface IGeneroService : IBaseService<Genero>
    {
        void AdicionarOuAtualizarViaProcedure(string procedure, Dictionary<string, string> parametros);
    }
}