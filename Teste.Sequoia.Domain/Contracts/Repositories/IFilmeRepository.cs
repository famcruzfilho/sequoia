﻿using System.Collections.Generic;
using Teste.Sequoia.Domain.Contracts.Repositories.Base;
using Teste.Sequoia.Domain.Models;

namespace Teste.Sequoia.Domain.Contracts.Repositories
{
    public interface IFilmeRepository : IBaseRepository<Filme>
    {
        void AdicionarOuAtualizarViaProcedure(string procedure, Dictionary<string, string> parametros);
    }
}