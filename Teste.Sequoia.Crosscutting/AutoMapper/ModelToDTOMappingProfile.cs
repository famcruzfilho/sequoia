﻿using AutoMapper;
using Teste.Sequoia.Domain.DTOs;
using Teste.Sequoia.Domain.Models;

namespace Teste.Sequoia.Crosscutting.AutoMapper
{
    public class ModelToDTOMappingProfile : Profile
    {
        protected void Configure()
        {
            CreateMap<Autor, AutorDTO>().ReverseMap();
            CreateMap<Filme, FilmeDTO>().ReverseMap();
            CreateMap<Genero, GeneroDTO>().ReverseMap();
        }
    }
}