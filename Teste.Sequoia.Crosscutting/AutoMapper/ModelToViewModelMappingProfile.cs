﻿using AutoMapper;
using Teste.Sequoia.Domain.Models;
using Teste.Sequoia.Domain.ViewModels;

namespace Teste.Sequoia.Crosscutting.AutoMapper
{
    public class ModelToViewModelMappingProfile : Profile
    {
        protected void Configure()
        {
            CreateMap<Autor, AutorViewModel>().ReverseMap();
            CreateMap<Filme, FilmeViewModel>().ReverseMap();
            CreateMap<Genero, GeneroViewModel>().ReverseMap();
        }
    }
}