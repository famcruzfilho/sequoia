﻿using System.Collections.Generic;

namespace Teste.Sequoia.Domain.DTOs
{
    public class AutorDTO
    {
        public int AutorId { get; set; }
        public string Nome { get; set; }
        public ICollection<FilmeDTO> Filmes { get; set; }
    }
}