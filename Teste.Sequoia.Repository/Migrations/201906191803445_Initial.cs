namespace Teste.Sequoia.Repository.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Autores",
                c => new
                    {
                        AutorId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 250, unicode: false),
                    })
                .PrimaryKey(t => t.AutorId);
            
            CreateTable(
                "dbo.Filmes",
                c => new
                    {
                        FilmeId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 250, unicode: false),
                        GeneroId = c.Int(nullable: false),
                        AutorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.FilmeId)
                .ForeignKey("dbo.Generos", t => t.GeneroId)
                .ForeignKey("dbo.Autores", t => t.AutorId)
                .Index(t => t.GeneroId)
                .Index(t => t.AutorId);
            
            CreateTable(
                "dbo.Generos",
                c => new
                    {
                        GeneroId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 250, unicode: false),
                    })
                .PrimaryKey(t => t.GeneroId);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Filmes", "AutorId", "dbo.Autores");
            DropForeignKey("dbo.Filmes", "GeneroId", "dbo.Generos");
            DropIndex("dbo.Filmes", new[] { "AutorId" });
            DropIndex("dbo.Filmes", new[] { "GeneroId" });
            DropTable("dbo.Generos");
            DropTable("dbo.Filmes");
            DropTable("dbo.Autores");
        }
    }
}