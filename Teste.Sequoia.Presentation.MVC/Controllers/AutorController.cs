﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Teste.Sequoia.Domain.Contracts.AppServices;
using Teste.Sequoia.Domain.ViewModels;

namespace Teste.Sequoia.Presentation.MVC.Controllers
{
    public class AutorController : Controller
    {
        private readonly IAutorAppService _autorAppService;

        public AutorController(IAutorAppService autorAppService)
        {
            _autorAppService = autorAppService;
        }

        [Route("index")]
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult Input()
        {
            return PartialView("_Input");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Salvar(AutorViewModel autor)
        {
            try
            {
                Dictionary<string, string> parametros = new Dictionary<string, string>();
                if(autor.AutorId > 0)
                {
                    parametros.Add("Operacao", "U");
                }
                else
                {
                    parametros.Add("Operacao", "I");
                }
                parametros.Add("AutorId", autor.AutorId.ToString());
                parametros.Add("Nome", autor.Nome);
                _autorAppService.AdicionarOuAtualizarViaProcedure("CadastroAutor", parametros);
                return Json(new { Sucesso = true, MensagemRetorno = "Autor salvo com sucesso." });
            }
            catch(Exception ex)
            {
                return Json(new { Sucesso = false, MensagemRetorno = ex.Message });
            }
        }
    }
}