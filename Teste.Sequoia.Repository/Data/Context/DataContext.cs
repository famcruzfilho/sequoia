﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Teste.Sequoia.Domain.Models;
using Teste.Sequoia.Repository.Data.Mappings;

namespace Teste.Sequoia.Repository.Data.Context
{
    public class DataContext : DbContext   
    {
        public DataContext() : base("name=DefaultConnection")
        {
            Configuration.ProxyCreationEnabled = true;
            Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Autor> Autores { get; set; }
        public DbSet<Filme> Filmes { get; set; }
        public DbSet<Genero> Generos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Properties().Where(x => x.Name == x.ReflectedType.Name + "Id").Configure(x => x.IsKey());
            modelBuilder.Properties<string>().Configure(x => x.HasColumnType("varchar"));
            modelBuilder.Properties<string>().Configure(x => x.HasMaxLength(500));
            modelBuilder.Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));

            //Mappings
            modelBuilder.Configurations.Add(new AutorMap());
            modelBuilder.Configurations.Add(new FilmeMap());
            modelBuilder.Configurations.Add(new GeneroMap());
        }
    }
}