﻿using AutoMapper;

namespace Teste.Sequoia.Crosscutting.AutoMapper
{
    public class AutoMapperConfig : Profile
    {
        public static void RegisterMappings()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<ModelToViewModelMappingProfile>();
                x.AddProfile<ModelToDTOMappingProfile>();
            });
        }
    }
}