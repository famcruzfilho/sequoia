﻿using System.Collections.Generic;

namespace Teste.Sequoia.Domain.ViewModels
{
    public class AutorViewModel
    {
        public int AutorId { get; set; }
        public string Nome { get; set; }
        public ICollection<FilmeViewModel> Filmes { get; set; }
    }
}