﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Teste.Sequoia.Domain.Models;

namespace Teste.Sequoia.Repository.Data.Mappings
{
    public class AutorMap : EntityTypeConfiguration<Autor>
    {
        public AutorMap()
        {
            ToTable("Autores");

            Property(x => x.AutorId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.Nome)
                .HasMaxLength(250)
                .IsRequired();

            HasMany<Filme>(s => s.Filmes);
        }
    }
}