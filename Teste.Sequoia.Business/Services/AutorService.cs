﻿using System.Collections.Generic;
using Teste.Sequoia.Business.Services.Base;
using Teste.Sequoia.Domain.Contracts.Repositories;
using Teste.Sequoia.Domain.Contracts.Services;
using Teste.Sequoia.Domain.Models;

namespace Teste.Sequoia.Business.Services
{
    public class AutorService : BaseService<Autor>, IAutorService
    {
        private readonly IAutorRepository _autorRepository;

        public AutorService(IAutorRepository autorRepository) : base(autorRepository)
        {
            _autorRepository = autorRepository;
        }

        public void AdicionarOuAtualizarViaProcedure(string procedure, Dictionary<string, string> parametros)
        {
            _autorRepository.AdicionarOuAtualizarViaProcedure(procedure, parametros);
        }
    }
}