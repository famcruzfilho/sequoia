﻿using System.Collections.Generic;
using Teste.Sequoia.AppService.AppServices.Base;
using Teste.Sequoia.Domain.Contracts.AppServices;
using Teste.Sequoia.Domain.Contracts.Services;
using Teste.Sequoia.Domain.Models;

namespace Teste.Sequoia.AppService.AppServices
{
    public class GeneroAppService : BaseAppService<Genero>, IGeneroAppService
    {
        private readonly IGeneroService _generoService;

        public GeneroAppService(IGeneroService generoService) : base(generoService)
        {
            _generoService = generoService;
        }

        public void AdicionarOuAtualizarViaProcedure(string procedure, Dictionary<string, string> parametros)
        {
            _generoService.AdicionarOuAtualizarViaProcedure(procedure, parametros);
        }
    }
}